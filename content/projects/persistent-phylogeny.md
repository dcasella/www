---
date: "2017-11-29T14:11:36+01:00"
title: "Persistent Phylogeny"
categories:
  - University
  - Pinned
tags:
  - Bash
  - C++
  - Python
---

> 2017 - 2018  
> [GitHub Repository](https://github.com/dcasella/persistent-phylogeny)

Thesis.

Implementation of the persistent phylogeny polynomial-time algorithm described in this [paper](https://arxiv.org/abs/1611.01017).
A detailed explaination of the implementation process is given in my [Bachelor's thesis](https://github.com/dcasella/bsc-thesis).
