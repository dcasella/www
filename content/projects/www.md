---
date: "2018-11-29T14:37:57+01:00"
title: "WWW"
categories:
  - Personal
tags:
  - CI-CD
  - Hugo
---

> 2018 - Present  
> [GitLab Repository](https://gitlab.com/dcasella/www)

DNS: Cloudflare.  
Hosting: GitLab Pages.  
Static site generator: Hugo.

Previous iterations:

- Rust Rocket:  
  Rocket web framework for Rust, which renders Markdown and TOML files as HTML; no database is needed to store information. A simple new/edit/delete web interface is provided during development (via `./run.sh dev`).  
- Python Flask:  
  Modules like *Flask-FlatPages* for storing (no database) and rendering pages, projects, and blog posts. Gunicorn, HTTP server, was installed inside the application's virtualenv.  
- Ruby on Rails
