---
date: "2017-11-29T14:11:04+01:00"
title: "Sistemi Embedded"
categories:
  - University
tags:
  - C
---

> 2017 - 2017  
> [GitHub Repository](https://github.com/dubvulture/sistemi_embedded)

University course: Embedded Systems.

Hardware: C8051F02X; Sensors: accelerometer, thermometer, LCD, all controlled via SMBus.
The project consists in the scheduling of the following tasks:

- continuos detection (100ms interval) of the angle of inclination on the axes X, Y, Z
- continuos detection (1s interval) of the external temperature
- continuos display (300ms interval) of the data
- ability to turn on, off, and adjust the brightness of the LCD's backlight
