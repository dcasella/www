---
date: "2017-11-29T14:09:43+01:00"
title: "Multivariate Polynomials"
categories:
  - University
tags:
  - Common Lisp
  - Prolog
---

> 2017 - 2017  
> [GitHub Repository](https://github.com/dcasella/multivariate-polynomials)

University course: Programming languages.

Implementation of a multivariate polynomials manipulation libraries for Common Lisp and Prolog.
The course taught functional and logical programming paradigms.
