---
date: "2018-11-29T14:19:32+01:00"
title: "Personal server"
categories:
  - Personal
tags:
  - Bash
  - CI-CD
  - System administration
---

> 2018 - Present  
> [GitLab Repository](https://gitlab.com/dcasella/server-conf)

**OS:** Debian 10.  
**Services:** Nextcloud, mail server, Teamspeak server, IPsec.

**NGINX:** configured for Nextcloud and Z-Push.  
**Nextcloud:** IMAP authentication; CalDAV & CardDAV.  
**Z-Push:** integration between Nextcloud's CalDAV and CardDAV, Postfix's SMTP and Dovecot's IMAP for ActiveSync support.

**Postfix:** TLS enabled; configured with support for Postfixadmin: multi-domain, virtual users and aliases, database SMTP authentication.  
**Dovecot:** TLS required; database IMAP authentication; support for sieve filters.  

Most of the system is managed by Puppet (standalone).  
Puppet modules don't always support every possibile configuration tweak: some configuration files are rsynced from the repository to the server via GitLab CI.
