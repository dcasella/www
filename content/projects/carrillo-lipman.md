---
date: "2017-11-29T14:10:08+01:00"
title: "Carrillo Lipman"
categories:
  - University
  - Pinned
tags:
  - C
---

> 2017 - 2017  
> [GitHub Repository](https://github.com/dcasella/carrillo-lipman)

University course: Introduction to Bioinformatics.

Implementation of the Carrillo-Lipman algorithm for aligning three sequences.
